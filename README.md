# Setup

 - Start Zookeeper, Kafka & ElasticSearch:  `docker-compose up`
 - Create Kafka topic: `docker-compose exec kafka kafka-topics --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic users`
 
 - Start Consumer & Producer e.g.: `sbt runMain Consumer` `sbt runMain Producer`

