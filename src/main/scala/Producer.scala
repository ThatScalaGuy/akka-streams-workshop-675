import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCode, StatusCodes}
import akka.kafka.ProducerMessage.MultiResultPart
import akka.kafka.{ProducerMessage, ProducerSettings}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Framing, Sink, Source}
import akka.util.ByteString
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}

import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

object Producer extends App {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  def requestMapper = Flow[String].map(url => (HttpRequest(uri = url), NotUsed))

  def httpClient: Flow[(HttpRequest, NotUsed), (Try[HttpResponse], NotUsed), NotUsed] = Http().superPool[NotUsed]()

  def responseHandler = Flow[(Try[HttpResponse], NotUsed)].collect {
    case (Success(response), _) if response.status == StatusCodes.OK =>
      Success(response.entity.dataBytes)

    case (Success(response), _) =>
      response.entity.dataBytes.to(Sink.ignore).run()
      Failure(new Exception("wrong status code"))
  }.collect {
    case Success(data) => data
  }

  def readCSV(data: Source[ByteString, Any]) = data.via(
    Framing.delimiter(
      ByteString("\n"),
      maximumFrameLength = 1024,
      allowTruncation = true
    )
      .drop(1)
      .map(_.utf8String)
  )

  val producerSettings = ProducerSettings(system, new StringSerializer, new StringSerializer)
    .withBootstrapServers("localhost:9092")

  def mapKafkaMsg = Flow[String].map { elem =>
    ProducerMessage.Message(
      new ProducerRecord[String, String]("users", elem),
      akka.NotUsed)
  }

  Source.tick(initialDelay = 0 seconds, interval = 5 seconds, tick = "https://randomuser.me/api/?results=10&inc=name,gender,nat&noinfo&nat=us,dk,fr,gb&format=csv")
    .via(requestMapper)
    .via(httpClient)
    .via(responseHandler)
    .flatMapConcat(readCSV)
    .via(mapKafkaMsg)
    .via(akka.kafka.scaladsl.Producer.flexiFlow(producerSettings))
    .map {
      case ProducerMessage.Result(metadata, message) =>
        val record = message.record
        s"${metadata.topic}/${metadata.partition} ${metadata.offset}: ${record.value}"

      case ProducerMessage.MultiResult(parts, passThrough) =>
        parts
          .map {
            case MultiResultPart(metadata, record) =>
              s"${metadata.topic}/${metadata.partition} ${metadata.offset}: ${record.value}"
          }
          .mkString(", ")

      case ProducerMessage.PassThroughResult(passThrough) =>
        s"passed through"
    }
    .runWith(Sink.foreach(println(_)))
}
