import akka.actor.ActorSystem
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink}
import org.apache.http.HttpHost
import org.apache.kafka.common.serialization.StringDeserializer
import org.elasticsearch.client.RestClient
import spray.json._
import DefaultJsonProtocol._
import akka.stream.alpakka.elasticsearch.IncomingMessage
import akka.stream.alpakka.elasticsearch.scaladsl.ElasticsearchFlow


object Consumer extends App {

  case class User(gender: String, title: String, firstName: String, lastName: String, nationality: String) {
    def id = this.toString
  }

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val client: RestClient = RestClient.builder(new HttpHost("localhost", 9200)).build()

  implicit val format: JsonFormat[User] = jsonFormat5(User)

  def mapToUser = Flow[String].map { data =>
    data.split(",") match {
      case Array(gender, title, firstName, lastName, nationality) => User(gender, title, firstName, lastName, nationality)
    }
  }

  def mapToEsDocument = Flow[User].map { user =>
    IncomingMessage(Some(user.id), user)
  }

  val consumerSettings = ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
    .withBootstrapServers("localhost:9092")
    .withGroupId("elastic-writer-consumer")

  akka.kafka.scaladsl.Consumer.committableSource(consumerSettings, Subscriptions.topics("users"))
    .map(_.record.value())
    .via(mapToUser)
    .via(mapToEsDocument)
    .via(ElasticsearchFlow.create[User](
      indexName = "idx_user",
      typeName = "user"
    ))
    .runWith(Sink.foreach(println))
}


